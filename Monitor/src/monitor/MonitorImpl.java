package monitor;

import java.sql.Timestamp;

import org.omg.CORBA.ORB;

import bank.*;

public class MonitorImpl extends MonitorPOA{
	ORB orb = null;
	MonitorImpl(ORB orb) {
		this.orb = orb;
	}
	
	@Override
	public void meldung(final String msg) {

		new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println(new Timestamp(System.currentTimeMillis()).toString()+": "+msg);
				
			}
		
		}
		).start();
		
	}

	@Override
	public void exit() {
		System.out.println("Montior wird beendet");
		class shutdown implements Runnable {
			ORB orb;
			shutdown(ORB orb) { this.orb = orb; } 
			public void run() {
				
				this.orb.shutdown(true);
				
			}
		};
		new Thread(new shutdown(this.orb)).start();
	}

}
