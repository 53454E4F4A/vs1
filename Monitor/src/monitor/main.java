package monitor;

import java.util.Properties;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import bank.Bank;
import bank.BankHelper;
import bank.Monitor;
import bank.MonitorHelper;

public class main {
	
	public static void main(String args[]) {
		try {
			Properties p = new Properties();
			p.put("org.omg.COBRA.ORBInitialPort",  "1050");
			p.put("org.omg.COBRA.ORBInitialHost", "localhost");
			ORB orb = ORB.init(args, p);
			
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			MonitorImpl mon = new MonitorImpl(orb);
		
			org.omg.CORBA.Object ref = rootPoa.servant_to_reference(mon);
			
			Monitor href = MonitorHelper.narrow(ref);
			
			System.out.println("monitor ready and waiting");
			NamingContextExt nc = NamingContextExtHelper.narrow(orb.resolve_initial_references("NameService"));
			org.omg.CORBA.Object obj = nc.resolve_str(args[0]);
			Bank bank = BankHelper.narrow(obj);
			bank.monitorHinzufuegen(href);
			orb.run();
			orb.destroy();
		} catch(Exception e) {
			System.err.println("ERROR: " + e);
			//e.printStackTrace(System.out);
		}
		System.out.println("monitor exiting");
	}
}
