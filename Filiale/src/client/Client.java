package client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import bank.*;
import bank.BankPackage.EKontoAlreadyExists;
import bank.BankPackage.EKontoNotFound;
import bank.KontoPackage.EInvalidAmount;
import bank.KontoPackage.ENotEnoughMoney;
enum operation {
	ende,
	liste,
	anlegen,
	loeschen,
	einzahlen,
	auszahlen,
	ueberweisen,
	error
}
public class Client {
	private final static int off = 1;
	public static void demo(Bank bank) {

		try {
			bank.neu("1");
			bank.neu("2");
			bank.hole("1").einzahlen(500);
		} catch (EKontoAlreadyExists | EInvalidAmount | EKontoNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	
	
	public static void main(String args[]) {
		String Bank = null;
		String Konto = null;
		Double Betrag = null;
		String BankTo = null;
		String KontoTo = null;
		int wdh = 1;
		operation op = operation.error;
		TKontolisteHolder holder = new TKontolisteHolder();
		Properties p = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream stream = loader.getResourceAsStream("host.properties");
//		try {
//			//p.load(stream);
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		p.setProperty("org.omg.CORBA.ORBInitialPort", "5000");
		p.setProperty("org.omg.CORBA.ORBInitialHost", args[0]);
		
		if(args.length == 2+off && args[off+0].equals("ende") ) {
			op = operation.ende;
			Bank = args[off+1];
		} else if(args.length == 2+off && args[off+0].equals("liste")) {
			op = operation.liste;
			Bank = args[off+1];
			
		} else if(args.length == 3+off && args[off+0].equals("anlegen")) {
			op = operation.anlegen;
			Bank = args[off+1];
			Konto = args[off+2];
		} else if(args.length == 3+off && args[off+0].equals("loeschen")) {
			op = operation.loeschen;
			Bank = args[off+1];
			Konto = args[off+2];
		} else if((args.length == 4+off||args.length == 5+off) && args[off+0].equals("einzahlen")) {
			op = operation.einzahlen;
			Bank = args[off+1];
			Konto = args[off+2];
			if(args.length==5+off) {
				wdh = Integer.parseInt(args[off+4]);
			}
			try {
				Betrag = Double.parseDouble(args[off+3]);
			} catch (NumberFormatException e) { 
				op = operation.error;
			}; 
		
		} else if(args.length == 4+off && args[off+0].equals("auszahlen")) {
			op = operation.auszahlen;
			Bank = args[off+1];
			Konto = args[off+2];
			try {
				Betrag = Double.parseDouble(args[off+3]);
			} catch (NumberFormatException e) { 
				op = operation.error;
			}; 
		} else if(args.length == 6+off && args[off+0].equals("ueberweisen")) {
			op = operation.ueberweisen;
			Bank = args[off+1];
			Konto = args[off+2];
			BankTo = args[off+3];
			KontoTo = args[off+4];
			try {
				Betrag = Double.parseDouble(args[off+5]);
			} catch (NumberFormatException e) { 
				op = operation.error;
			}; 
		} else {
			op = operation.error;
			System.out.println(
					 "java client.Client <namensdienst> ende <bankname>\n"+
				     "java client.Client <namensdienst> liste <bankname>\n"+
				     "java client.Client <namensdienst> anlegen <bankname> <kontonummer>\n"+
				     "java client.Client <namensdienst> loeschen <bankname> <kontonummer>\n"+
				     "java client.Client <namensdienst> einzahlen <bankname> <kontonummer> <betrag>\n"+
				     "java client.Client <namensdienst> auszahlen <bankname> <kontonummer> <betrag>\n"+
				     "java client.Client <namensdienst> ueberweisen <vonbankname> <vonkontonummer> <nachbankname> <nachkontonummer> <betrag>\n");
					
			return;
		}
		
		
		try {

			ORB orb = ORB.init(args, p);
			NamingContextExt nc = NamingContextExtHelper.narrow(orb.resolve_initial_references("NameService"));
			org.omg.CORBA.Object obj = nc.resolve_str(Bank);
			
			Bank bank = BankHelper.narrow(obj);
		
			
			switch(op) {
				case ende:
					bank.exit();
					break;
				case liste:
					bank.getKontoliste(holder);
					for(Konto k : holder.value) {
						System.out.println("Kontonr: " + k.kontonr() + " stand: " + k.kontostand());
					}
					break;
				case anlegen:
					bank.neu(Konto);
					break;
				case loeschen:
					bank.loeschen(Konto);
					break;
				case einzahlen:
					Konto k = bank.hole(Konto);
					long time = System.currentTimeMillis();
					for(int i = 0; i < wdh; i++) k.einzahlen(Betrag);
						System.out.println("Average time per einzahlung: "+ ((double)(System.currentTimeMillis()-time))/wdh);
					break;
				case auszahlen:
					bank.hole(Konto).auszahlen(Betrag);
					break;
				case ueberweisen:
					org.omg.CORBA.Object obj2 = nc.resolve_str(BankTo);
					Bank bank2 = BankHelper.narrow(obj2);
					bank.hole(Konto).transfer(Betrag, bank2.hole(KontoTo));
					break;
				case error:
				
					break;
					
			}
			orb.destroy();
		
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		} catch (EKontoAlreadyExists e) {
			
			System.out.println("Konto existiert bereits");
		} catch (EKontoNotFound e) {
			System.out.println("Konto existiert nicht");
			
		} catch (EInvalidAmount e) {
			System.out.println("ungueltiger Betrag");
		} catch (ENotEnoughMoney e) {
			System.out.println("nicht genug Geld");
		
		} catch (org.omg.CORBA.UserException e) {
			System.out.println("UserException");
		} catch (org.omg.CORBA.SystemException e) {
			System.out.println("SystemException");
			e.printStackTrace();
		} 
		
		
		//orb.shutdown(true);
		
		
	}
}
