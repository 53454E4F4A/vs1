package server;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.omg.CORBA.ORB;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAPackage.ObjectNotActive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongAdapter;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import bank.*;
import bank.BankPackage.EKontoAlreadyExists;
import bank.BankPackage.EKontoNotFound;

public class BankImpl extends BankPOA {


	private ORB orb;
	private HashMap<String, org.omg.CORBA.Object> kontoliste;
	private POA rootPoa;	
	private List<Monitor> monitors;
	private MonitorThread monthread;

	public void Bankinit(ORB o, POA rootPoa) {
		this.orb = o;
		this.rootPoa = rootPoa;
		this.kontoliste = new HashMap<String, org.omg.CORBA.Object>();
		this.monitors = Collections.synchronizedList(new LinkedList<Monitor>());
		monthread = new MonitorThread(this.monitors);
		new Thread(monthread).start();
	}
	@Override
	public int getKontoliste(TKontolisteHolder kontoliste) {
		List<Konto> kontos = new LinkedList<Konto>();
		for( org.omg.CORBA.Object ref : this.kontoliste.values()) {
			kontos.add(KontoHelper.narrow(ref));
		}
		kontoliste.value = kontos.toArray(new Konto[0]);
		return kontos.size();		
	}

	@Override
	public Konto neu(String kontonr) throws EKontoAlreadyExists {
		KontoImpl konto = null;
		Konto href = null;
		org.omg.CORBA.Object ref = null;
		System.out.println("neues konto");
		if(this.kontoliste.containsKey(kontonr)) {
			monitorsMeldung("neu EKontoAlreadyExists: " + kontonr);
			throw new EKontoAlreadyExists("already exists");
		} else {
			
		
		konto = new KontoImpl(kontonr, monthread);
		try {
			ref = rootPoa.servant_to_reference(konto);	
			kontoliste.put(kontonr, ref);
			href = KontoHelper.narrow(ref);
			monitorsMeldung("Konto: " + kontonr + " erstellt");
		} catch (ServantNotActive | WrongPolicy e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			}
		}
		
		return href;
	}
	
	@Override
	public void loeschen(String kontonr) throws EKontoNotFound {
		if(!kontoliste.containsKey(kontonr)) {
			monitorsMeldung("loeschen EKontoNotFound " + kontonr );
			throw new EKontoNotFound("Konto nicht gefunden");
		}
		try {
			
			rootPoa.deactivate_object(rootPoa.reference_to_id(kontoliste.remove(kontonr)));		
			monitorsMeldung("Konto " + kontonr + " gelöscht");
		} catch (WrongAdapter | WrongPolicy | ObjectNotActive e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	@Override
	public Konto hole(String kontonr) throws EKontoNotFound {
		if(!kontoliste.containsKey(kontonr)) throw new EKontoNotFound("Konto nicht gefunden");
		return KontoHelper.narrow(kontoliste.get(kontonr));
	}

	@Override
	public void monitorHinzufuegen(Monitor theMonitor) {
	
		
		theMonitor.meldung("Monitor hinzugefügt ("+this.monitors.size()+")");
		this.monitors.add(theMonitor);
	}

	@Override
	public void monitorEntfernen(Monitor theMonitor) {
		theMonitor.meldung("Monitor entfernt ("+this.monitors.size()+")");
		this.monitors.remove(theMonitor);
		
	}
	public void monitorsMeldung(String msg) {
//		for(Monitor m : this.monitors) {
//			m.meldung(msg);
//		}
		monthread.MessageQ.add(msg);
	}
	@Override
	public void exit() {
		
		monthread.MonitorTerminate();
		monthread.MessageQ.add("terminating sever!");
		for(Monitor m : this.monitors) {
			try {
			m.exit();
			} catch ( org.omg.CORBA.COMM_FAILURE e) {
				System.out.println("comfailure");
			}
		}
		//orb.shutdown(false);
		class shutdown implements Runnable {
			ORB orb;
			POA rootpoa;
			shutdown(ORB orb, POA rootPoa) { this.orb = orb; this.rootpoa = rootPoa; } 
			public void run() {
				this.rootpoa.destroy(true, true);
				this.orb.shutdown(true);
				
			}
		};
		new Thread(new shutdown(this.orb, this.rootPoa)).start();
		
	}

}