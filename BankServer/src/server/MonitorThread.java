package server;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.text.html.HTMLDocument.Iterator;

import bank.Monitor;

public class MonitorThread implements Runnable {
	private List<Monitor> monitors;
	public BlockingQueue<String> MessageQ = null; 
	private boolean running = true;
	public MonitorThread(List<Monitor> monitors) {
		super();
		this.monitors = monitors;
		MessageQ = new LinkedBlockingQueue<String>();
	}
	
	public void MonitorTerminate() {
		running = false;
	}
 	@Override
	public void run() {
 		String message = null;
		while(running) {
			try {
				message = MessageQ.take();
				 //synchronized(monitors) {
				      java.util.Iterator<Monitor> i = monitors.iterator();
				      while (i.hasNext())
				         i.next().meldung(message);
				 // }
			} catch (Exception e) {
				System.out.println("MonThread interrupted");
			}
			
			
		}
	}

}
