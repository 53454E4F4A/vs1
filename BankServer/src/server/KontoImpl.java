package server;
import java.util.List;

import bank.*;
import bank.KontoPackage.EInvalidAmount;
import bank.KontoPackage.ENotEnoughMoney;


public class KontoImpl extends KontoPOA{
	private String kontonr;
	private double kontostand;
	private MonitorThread mt;
	KontoImpl(String kontonr, MonitorThread mt) {
		this.kontonr = kontonr;
		this.kontostand = 0.0;
		this.mt = mt;
	}
	
	@Override
	public double kontostand() {
		// TODO Auto-generated method stub
		return this.kontostand;
	}

	@Override
	public String kontonr() {
		// TODO Auto-generated method stub
		return this.kontonr;
	}

	@Override
	public void einzahlen(double betrag) throws EInvalidAmount {
		if(betrag <= 0 || ((Double.MAX_VALUE-betrag)-this.kontostand) < 0) {
			throw new EInvalidAmount("ungueltiger Betrag");
		} else {
			
				mt.MessageQ.add("Konto " + this.kontonr + ": +" + betrag + " from " + this.kontostand + " to " + (this.kontostand + betrag));
			
			this.kontostand += betrag;
		}
		return;
	}

	@Override
	public void auszahlen(double betrag) throws EInvalidAmount, ENotEnoughMoney {
		if(betrag > this.kontostand) {
			throw new ENotEnoughMoney("nicht genug Geld");
		} else if(betrag <= 0) {
			throw new EInvalidAmount("ungeueltiger Betrag");
		} else {
			this.kontostand -= betrag;
			mt.MessageQ.add("Konto " + this.kontonr + ": -" + betrag + " from " + this.kontostand + " to " + (this.kontostand + betrag));
			}
		}
		
	

	@Override
	public void transfer(double betrag, Konto toKonto) throws EInvalidAmount {
		try {
			
				mt.MessageQ.add("Konto " + this.kontonr + ": -" + betrag + " from " + this.kontostand + " to " + (this.kontostand - betrag));
				mt.MessageQ.add("Konto " + toKonto.kontonr() + ": +" + betrag + " from " + toKonto.kontostand() + " to " + (toKonto.kontostand() + betrag));
			
			this.auszahlen(betrag);
			toKonto.einzahlen(betrag);
		} catch (ENotEnoughMoney e) {
			System.out.println("ENotEnoughMoney");
			throw new EInvalidAmount("ungueltiger Betrag");
		}
	
		
	}

}
