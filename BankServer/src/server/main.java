package server;

import java.util.Properties;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import bank.*;


public class main {
	public static void main (String args[]) {
		try {
			Properties p = new Properties();
			p.put("org.omg.COBRA.ORBInitialPort",  "1050");
			p.put("org.omg.COBRA.ORBInitialHost", "localhost");
			ORB orb = ORB.init(args, p);
			
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			final BankImpl bank = new BankImpl();
			bank.Bankinit(orb,  rootPoa);
			org.omg.CORBA.Object ref = rootPoa.servant_to_reference(bank);
			
			Bank href = BankHelper.narrow(ref);
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
			
			String name = args[0];
			NameComponent path[] = ncRef.to_name( name );
			ncRef.rebind(path, href);
		
			System.out.println("server ready and waiting");
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					bank.exit();
				
				}
				
			}));
			orb.run();
			
			
		} catch(Exception e) {
	
		//	System.err.println("ERROR: " + e);
			//e.printStackTrace(System.out);
		}
		System.out.println("server exiting");
		return;
	}
}
